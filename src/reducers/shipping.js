import {SET_SUBMIT_DATA} from "../consts";

const initialState = {
    submitData: null
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_SUBMIT_DATA:
            return {
                ...state,
                submitData:  {...action.payload}
            };
        default:
            return state;
    }
}