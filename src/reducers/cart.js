import {REMOVE_PRODUCT_FROM_CART, SET_PRODUCTS, UPDATE_PRODUCT_COUNT} from '../consts'

const initialState = {
    isReady: false,
    products: null,
    price: 0
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_PRODUCTS:
            const setPrice = (products) => {
                if (products > 0)
                    return action.payload.reduce((sum, item) => sum + parseFloat(item.price) * item.count, 0).toFixed(2);
                return 0
            };
            return {
                ...state,
                products: action.payload,
                isReady: true,
                price: setPrice(action.payload.length)
            };

        case REMOVE_PRODUCT_FROM_CART: {
            return {
                ...state,
                products: state.products.filter(o => o.id !== action.payload.id),
                price: parseFloat(state.price) - action.payload.price * action.payload.count
            }
        }
        case UPDATE_PRODUCT_COUNT:{
            return {
                ...state,
                price: state.products.reduce((sum, item) => sum + parseFloat(item.price) * item.count, 0).toFixed(2)
            }
        }
        default:
            return state;
    }
}