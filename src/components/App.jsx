import React from 'react';
import style from '../styles/app.module.css';
import Cart from "../containers/Cart";
import {Redirect, Route} from "react-router-dom";
import ShippingContainer from '../containers/Shipping'
import {SkeletonCart} from "./Skeleton/SkeletonCart";

const App = ({isReady}) => {
    return (
        <div className={style.container}>
            {!isReady ?
                <SkeletonCart/>
                :
                <>
                    <Redirect exact from="/" to="cart"/>
                    <Route path='/cart' component={Cart}/>
                    <Route path='/shipping' component={ShippingContainer}/>
                </>
            }
        </div>
    );
};

export default App;


