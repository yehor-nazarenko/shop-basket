import React from 'react';
import Skeleton from 'react-loading-skeleton';
import SkeletonItem from "./SkeletonItem";
import style from "../../styles/cart.module.css"

export const SkeletonCart = () => {
    return (
        <div className={style.mainContainer}>
            <div className={style.secContainer}>
                <ul>
                    <SkeletonItem/>
                    <SkeletonItem/>
                    <SkeletonItem/>
                    <SkeletonItem/>
                    <SkeletonItem/>
                    <SkeletonItem/>
                    <SkeletonItem/>
                </ul>
                <div className={style.price}>
                    <Skeleton/>
                </div>
            </div>
        </div>
    )
};


