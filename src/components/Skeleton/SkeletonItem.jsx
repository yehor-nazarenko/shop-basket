import React from 'react';
import style from "../../styles/item.module.css"
import Skeleton from 'react-loading-skeleton';

const SkeletonItem = () => {
    return (
        <li className={style.item}>
                <div className={style.link + ' ' + style.img}>
                    <Skeleton width={80} height={80}/>
                </div>

            <div className={style.description}>
                <h2>
                    <Skeleton width={250} height={30}/>
                </h2>
                <div>
                    <Skeleton width={100}/>
                </div>
                <div>
                    <Skeleton width={100}/>
                </div>
            </div>
            <div className={style.rightBlock}>
                <div className={style.quantity}>
                    <Skeleton width={100} height={30}/>
                </div>
                <div className={style.price}>
                    <button
                        type="button">
                    </button>
                    <div>
                        <Skeleton width={100} height={30}/>
                    </div>
                </div>
            </div>
        </li>
    )
};
export default SkeletonItem;


