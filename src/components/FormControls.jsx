import React from "react";
import styles from "../styles/formControls.module.css"


export const Input = ({input, type, label, meta}) => {
    const hasError = meta.touched && meta.error;
    return (
        <div>
            <div>
                <input {...input} type={type} placeholder={label}
                       className={"form-control" + " " + (hasError ? styles.invalid : "")}/>
            </div>
            {hasError &&
            <div className={styles.invalidTooltip}>{meta.error}</div>}
        </div>
    )
};


