import React from 'react';
import style from "../styles/stepper.module.css"

const Stepper = ({product, count, updateCount}) => {

    const increaseCount = (product) => {
        if (product.count < 50) {
            updateCount(product.id, ++product.count);
        }
    };
    const reduceCount = (product) => {
        if (product.count > 1) {
            updateCount(product.id, --product.count);
        }
    };

    return (
        <div className={style.container}>
            <button
                type="button" onClick={reduceCount.bind(this, product)}>
                −
            </button>
            <input type="number" value={count} step="1" min="1" max="50" readOnly/>
            <button
                type="button" onClick={increaseCount.bind(this, product)}>
                +
            </button>
        </div>
    )
};
export default Stepper;


