import React from 'react';
import Item from "../containers/Item";
import style from "../styles/cart.module.css"
import {NavLink} from "react-router-dom";

const Cart = ({products, isReady, price}) => {
    return (
        <div className={style.mainContainer}>
            {!isReady ? <div>Loading</div>
                :
                <div className={style.secContainer}>
                    <ul>
                        {products.map(product =>
                            <Item key={product.id}
                                  product={product}
                            />
                        )}
                    </ul>
                    <div className={style.price}>
                        {price} &#8364;
                    </div>
                    <NavLink to="/shipping" className={style.buy}>
                        <button>Buy</button>
                    </NavLink>

                </div>
            }
        </div>
    );
};

export default Cart;


