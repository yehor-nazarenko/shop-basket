import React from 'react';
import {Field, formValueSelector, reduxForm} from 'redux-form'
import {Input} from "./FormControls";
import {alphaNumeric, email, maxLength30, maxLength50, minLength3, phoneNumber, required} from "../helpers/validators";
import style from "../styles/shipping.module.css"
import styleB from "../styles/cart.module.css"
import {connect} from "react-redux";


let Shipping = ({handleSubmit, price, products, shippingMethodValue, submitting, pristine}) => {

    const deliveryPrice = (value) => {
        switch (value) {
            case 'free shipping - 0':
                return 0;
            case 'express shipping - 9.99':
                return 9.99;
            case 'courier shipping - 19.99':
                return 19.99;
            case 'free express shipping - 0':
                return 0;
        }
    };

    const sumShippingAndPrice = () => {
        return deliveryPrice(shippingMethodValue) + parseFloat(price);
    };

    const onPriceAbove300 = (value) => {
        if (parseInt(value) > 300) {
            shippingMethodValue = 'free express shipping - 0';
            return "free express shipping 0";
        } else
            return shippingMethodValue;
    };
    return (
        <form onSubmit={handleSubmit} className={style.container}>
            <div className={style.item}>
                <label>Name</label>
                <div>
                    <Field
                        name="name"
                        component={Input}
                        validate={[required, minLength3, maxLength50, alphaNumeric]}
                        type="text"
                        label="name"
                    />
                </div>
            </div>
            <div className={style.item}>
                <label>Address</label>
                <div>
                    <Field
                        name="address"
                        component={Input}
                        validate={[required, minLength3, maxLength30]}
                        type="text"
                        label="address">
                    </Field>
                </div>
            </div>
            <div className={style.item}>
                <label>Phone</label>
                <div>
                    <Field
                        name="phone"
                        component={Input}
                        validate={[required, phoneNumber]}
                        type="number"
                        label="phone"
                    />
                </div>
            </div>
            <div className={style.item}>
                <label>Email</label>
                <div>
                    <Field
                        name="email"
                        component={Input}
                        validate={[required, email]}
                        type="email"
                        label="email"
                    />
                </div>
            </div>
            <div className={style.item}>
                <label>Shipping options</label>
                <div>
                    <Field name="shippingMethod" component="select">
                        <option value={'free shipping - 0'}>FREE SHIPPING</option>
                        <option value={'express shipping - 9.99'}>EXPRESS SHIPPING - 9.99 &#8364;</option>
                        <option value={'courier shipping - 19.99'}>COURIER SHIPPING - 19.99 &#8364;</option>
                    </Field>
                </div>
            </div>
            <div className={style.price}>
                {products.reduce((sum, item) => sum + item.count, 0)} amount of goods {price} &#8364;
            </div>
            {shippingMethodValue &&
            (<div className={style.price}>
                    {onPriceAbove300(price)} &#8364;
                </div>
            )}
            <div className={style.price + ' ' + style.total}>
                Total {!shippingMethodValue ? price : sumShippingAndPrice().toFixed(2)} &#8364;
            </div>
            <div className={styleB.buy}>
                <button type="submit" disabled={pristine || submitting}>
                    Submit
                </button>
            </div>
        </form>
    )
};

Shipping = reduxForm({
    form: 'shipping',
    initialValues: {
        'shippingMethod': 'free shipping - 0'
    }
})(Shipping);


const selector = formValueSelector('shipping');

Shipping = connect(state => {
    const shippingMethodValue = selector(state, 'shippingMethod');

    return {
        shippingMethodValue
    }
})(Shipping);

export default Shipping;
