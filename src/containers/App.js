import React, {useEffect} from 'react';
import '../styles/app.module.css';
import App from "../components/App";
import {connect} from "react-redux";
import {setProducts} from "../thunks/cart";


const AppContainer = (props) => {

    useEffect(() => {
        props.setProducts();
    }, [props.isReady]);

    return <App isReady={props.isReady}/>
};

const mapStateToProps = (state) => ({
    isReady: state.cart.isReady,
    products: state.cart.products
});

export default connect(mapStateToProps,
    {
        setProducts
    })(AppContainer)
