import * as axios from "axios";

const instance = axios.create({
    baseURL: 'https://5e3c30f3f2cb300014391c39.mockapi.io/api/v1'
});


export const cartAPI = {
    getItems() {
        return instance.get('item').then(response => response.data)
    },

    removeItem(itemID) {
        return instance.delete(`item/${itemID}`).then(response => response.data )
    },

    updateItem(itemID, count){
        return instance.put(`item/${itemID}`, {count:count})
            .then(
                response => response.data
            )
    }
};

