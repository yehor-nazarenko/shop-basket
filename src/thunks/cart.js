import {cartAPI} from "../api/api";
import {removeItemFromCart, setUserProducts, updateItem} from "../actions/cart";


export const setProducts = () => async (dispatch) => {
        const data = await cartAPI.getItems();
        if (data) {
            dispatch(setUserProducts(data));
        }
    }
;

export const deleteItemFromCart = (item) => async (dispatch) => {
    const data = await cartAPI.removeItem(item.id);
    if (data) {
        dispatch(removeItemFromCart(item));
    }
};


export const updateCount = (id, count) => async (dispatch) => {
    debugger;
        const data = await cartAPI.updateItem(id, count);

        if (data) {
            dispatch(updateItem(id));
        }
    }
;