import {REMOVE_PRODUCT_FROM_CART, SET_PRODUCTS, SET_SUBMIT_DATA, UPDATE_PRODUCT_COUNT} from '../consts'

export const setUserProducts = items => ({
    type: SET_PRODUCTS,
    payload: items
});

export const removeItemFromCart = item => ({
    type: REMOVE_PRODUCT_FROM_CART,
    payload: item
});

export const updateItem = id => ({
    type: UPDATE_PRODUCT_COUNT,
    payload: id
});

export const setSubmitData = data => ({
    type: SET_SUBMIT_DATA,
    payload: data
});
