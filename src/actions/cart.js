import {REMOVE_PRODUCT_FROM_CART, SET_PRODUCTS, UPDATE_PRODUCT_COUNT} from '../consts'

export const setUserProducts =  items => ({
    type: SET_PRODUCTS,
    payload: items
});

export const removeItemFromCart = item =>( {
    type: REMOVE_PRODUCT_FROM_CART,
    payload: item
});

export const updateItem = id =>( {
    type: UPDATE_PRODUCT_COUNT,
    payload: id
});

