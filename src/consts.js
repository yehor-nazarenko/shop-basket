export const SET_PRODUCTS = 'SET_ITEMS';
export const UPDATE_PRODUCT_COUNT = 'UPDATE_PRODUCT_COUNT';
export const REMOVE_PRODUCT_FROM_CART = 'REMOVE_PRODUCT_FROM_CART';
export const SET_SUBMIT_DATA = 'SET_SUBMIT_DATA';

